-include .env
export

install: proto app_build

proto:
	protoc --go_out=plugins=grpc:internal protobuf/banner_rotation_service/rotation.proto
	protoc --go_out=plugins=grpc:internal protobuf/analytics_service/analytics.proto

app_build:
	cp -n .env.dist .env || \
	go mod vendor
	go build -v -a -mod=vendor -installsuffix cgo -o ./main ./cmd/banner-rotation/main.go
	chmod +x ./main

dev_env_up:
	docker-compose -f docker-compose.yml down
	docker-compose -f docker-compose.yml up -d --build banner_grpc

fmt:
	go fmt ./...
	gofmt -w -s internal
	gofmt -w -s test

lint:
	golangci-lint run --enable-all --disable="gochecknoglobals,gochecknoinits,scopelint"

test_unit:
	go test -race -count 1 -v -timeout 10s -run Unit ./...

test_integration:
	go test -race -count 1 -v -timeout 60s -run Integration ./...

run_test:
	go mod vendor
run_test: test_unit test_integration

migrate:
	bin/migrate -source file:db/migrations -database 'mysql://${DB_USERNAME}:${DB_PASSWORD}@tcp(${DB_HOST}:${DB_PORT})/${DB_NAME}' up

migrate_fresh:
	bin/migrate -source file:db/migrations -database 'mysql://root:${DB_ROOT_PASSWORD}@tcp(${DB_HOST}:${DB_PORT})/${DB_NAME}' down -all
	bin/migrate -source file:db/migrations -database 'mysql://root:${DB_ROOT_PASSWORD}@tcp(${DB_HOST}:${DB_PORT})/${DB_NAME}' up

test_up:
	docker-compose -f docker-compose.test.yml up --abort-on-container-exit --build
	docker-compose -f docker-compose.test.yml down

evans:
	evans --host=localhost --port=${BANNER_GRPC_PORT} --package=proto ./protobuf/banner_rotation_service/rotation.proto