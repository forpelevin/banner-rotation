package main

import (
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"gitlab.com/forpelevin/banner-rotation/internal/metrics"

	"github.com/go-redis/redis/v7"
	grpc_prometheus "github.com/grpc-ecosystem/go-grpc-prometheus"
	"github.com/joho/godotenv"
	"github.com/pkg/errors"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/segmentio/kafka-go"
	"gitlab.com/forpelevin/banner-rotation/internal/cache"
	"gitlab.com/forpelevin/banner-rotation/internal/database"
	banner_rotation_proto "gitlab.com/forpelevin/banner-rotation/internal/protobuf/banner_rotation_service"
	"gitlab.com/forpelevin/banner-rotation/internal/queue"
	"gitlab.com/forpelevin/banner-rotation/internal/repository"
	"gitlab.com/forpelevin/banner-rotation/internal/resolver"
	"gitlab.com/forpelevin/banner-rotation/internal/service"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func main() {
	// if we crash the go code, we get the file name and line number
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	// Read the env file.
	err := godotenv.Load(".env")
	if err != nil {
		log.Fatal(err)
	}

	// Initialize your gRPC server's interceptor.
	s := NewGRPCServer()
	defer s.Stop()

	// After all your registrations, make sure all of the Prometheus metrics are initialized.
	grpc_prometheus.EnableHandlingTimeHistogram()
	grpc_prometheus.Register(s)
	// Register Prometheus metrics handler.
	http.Handle("/metrics", promhttp.Handler())

	// Register reflection service on gRPC server.
	reflection.Register(s)

	// Start the gRPC server.
	grpcAddr := fmt.Sprintf(
		"%s:%s",
		os.Getenv("BANNER_GRPC_LISTEN_HOST"),
		os.Getenv("BANNER_GRPC_PORT"),
	)
	lis, err := net.Listen("tcp", grpcAddr)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	defer lis.Close()

	go func() {
		fmt.Printf("Listening on: %s\n", os.Getenv("BANNER_GRPC_PORT"))
		if err := s.Serve(lis); err != nil {
			log.Fatalf("failed to serve: %v", err)
		}
	}()

	// Start the HTTP server for Prometheus metrics pulling.
	promLisAddr := fmt.Sprintf(
		"%s:%s",
		os.Getenv("BANNER_METRICS_LISTEN_HOST"),
		os.Getenv("BANNER_METRICS_LISTEN_PORT"),
	)
	go func() {
		fmt.Printf("Listening on: %s for Prometheus\n", promLisAddr)
		log.Fatal(http.ListenAndServe(promLisAddr, nil))
	}()

	// Wait for Control C to exit
	ch := make(chan os.Signal, 1)
	signal.Notify(ch, os.Interrupt)

	// Block until a signal is received
	<-ch
	fmt.Printf("Stopping the server...\nBye!\n")
}

// NewGRPCServer creates a new GRPC server instance.
func NewGRPCServer() *grpc.Server {
	s := grpc.NewServer(
		grpc.UnaryInterceptor(
			grpc_prometheus.UnaryServerInterceptor,
		),
	)

	sqlx, err := database.NewDBConnection()
	if err != nil {
		log.Fatal(err)
	}

	redisCl := redis.NewClient(&redis.Options{
		Addr: fmt.Sprintf("%s:%s", os.Getenv("REDIS_HOST"), os.Getenv("REDIS_PORT")),
	})
	_, err = redisCl.Ping().Result()
	if err != nil {
		log.Fatal(errors.Wrap(err, "failed to connect to Redis"))
	}

	kafkaAddress := fmt.Sprintf("%s:%s", os.Getenv("KAFKA_HOST"), os.Getenv("KAFKA_PORT"))
	kw := kafka.NewWriter(kafka.WriterConfig{
		Brokers:  []string{kafkaAddress},
		Topic:    queue.AnalyticsEventTopic,
		Balancer: &kafka.RoundRobin{},
	})

	db := repository.NewDB(sqlx)
	rds := cache.NewRedis(redisCl)
	kfk := queue.NewKafka(kw)
	ucb1 := resolver.NewUCB1(db, rds)

	// Prometheus metrics.
	cc := promauto.NewCounter(prometheus.CounterOpts{
		Name: "banner_rotation_clicks_total",
		Help: "The total number of clicks on banners",
	})
	ic := promauto.NewCounter(prometheus.CounterOpts{
		Name: "banner_rotation_impressions_total",
		Help: "The total number of banners impressions",
	})

	// Register our gRPC service.
	banner_rotation_proto.RegisterRotationServiceServer(
		s,
		service.NewRotationService(
			db,
			rds,
			kfk,
			ucb1,
			metrics.NewMetricCollector(cc, ic),
		),
	)

	return s
}
