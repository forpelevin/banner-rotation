# Banner rotation service
Реализация сервиса ротации баннеров по ТЗ https://github.com/OtusTeam/Go/blob/master/project-banners.md

## Установка
```
make install
``` 
Далее необходимо поднять окружение
```
make dev_env_up
```
Дожидаемся, пока база закончит инициализацию и прогоняем миграции
```
make migrate
```

## Тесты
В проекте есть как Юнит, так и интеграционные тесты. Их можно запускать командами:
```
make test_unit
make test_integration
make run_test // Накатит миграции и прогонит все тесты
```

## Improvements
Все ниже было опущено вследствие недостатка времени ;)
1) Добавить больше метрик в Prometheus. Выводить все в Grafana
2) Прокинуть кастомные настройки MariaDB, Redis, Kafka
3) Удалять из Redis всю инфу о баннере, когда его удаляют из ротации

## License
This project is licensed under the MIT License.
