package validator

import (
	"errors"

	"gitlab.com/forpelevin/banner-rotation/internal/repository"
)

var (
	ErrNullBannerSlot = errors.New("banner slot cannot be empty")
	ErrEmptyBannerID  = errors.New("banner ID is required")
	ErrEmptySlotID    = errors.New("slot ID is required")
	ErrEmptySdgroupID = errors.New("soc dem group ID is required")
)

// ValidateBannerSlot checks that the banner slot entity is valid and returns an error if its not.
func ValidateBannerSlot(bs *repository.BannerSlot) error {
	if bs == nil {
		return ErrNullBannerSlot
	}

	if bs.BannerID == 0 {
		return ErrEmptyBannerID
	}

	if bs.SlotID == 0 {
		return ErrEmptySlotID
	}

	return nil
}
