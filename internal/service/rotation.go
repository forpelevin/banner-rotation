package service

import (
	"context"
	"log"
	"time"

	"gitlab.com/forpelevin/banner-rotation/internal/metrics"

	"github.com/golang/protobuf/ptypes/timestamp"
	"github.com/pkg/errors"
	"gitlab.com/forpelevin/banner-rotation/internal/cache"
	analytics_proto "gitlab.com/forpelevin/banner-rotation/internal/protobuf/analytics_service"
	banner_rotation_proto "gitlab.com/forpelevin/banner-rotation/internal/protobuf/banner_rotation_service"
	"gitlab.com/forpelevin/banner-rotation/internal/queue"
	"gitlab.com/forpelevin/banner-rotation/internal/repository"
	"gitlab.com/forpelevin/banner-rotation/internal/resolver"
	"gitlab.com/forpelevin/banner-rotation/internal/validator"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

var (
	errEmptyReq = errors.New("the request cannot be empty")
)

// RotationService is an implementation of the protobuf Rotation Service Server. It allows to do next actions:
// 1) Add a banner to rotation
// 2) Delete a banner from rotation.
// 3) Handle a click on a banner in rotation.
// 4) Get a banner from rotation for given social demographic group in a slot.
type RotationService struct {
	repo  repository.BannerRotationRepository
	cache cache.BannerCache
	aq    queue.AnalyticsQueue
	br    resolver.BannerResolver
	mc    *metrics.MetricCollector
}

// NewRotationService is a constructor.
func NewRotationService(
	repo repository.BannerRotationRepository,
	cache cache.BannerCache,
	aq queue.AnalyticsQueue,
	br resolver.BannerResolver,
	mc *metrics.MetricCollector,
) *RotationService {
	return &RotationService{
		repo:  repo,
		cache: cache,
		aq:    aq,
		br:    br,
		mc:    mc,
	}
}

// AddBanner adds a banner to rotation.
func (rs *RotationService) AddBanner(ctx context.Context, r *banner_rotation_proto.AddBannerRequest) (*banner_rotation_proto.AddBannerResponse, error) {
	bs := &repository.BannerSlot{
		BannerID: r.BannerId,
		SlotID:   r.SlotId,
	}

	err := validator.ValidateBannerSlot(bs)
	if err != nil {
		return nil, err
	}

	err = rs.repo.StoreBannerSlot(ctx, bs)
	if err != nil {
		return nil, status.Errorf(
			codes.Internal,
			err.Error(),
		)
	}

	return &banner_rotation_proto.AddBannerResponse{}, nil
}

// DeleteBanner deletes a banner from rotation.
func (rs *RotationService) DeleteBanner(ctx context.Context, r *banner_rotation_proto.DeleteBannerRequest) (*banner_rotation_proto.DeleteBannerResponse, error) {
	if r == nil {
		return nil, errEmptyReq
	}

	if r.BannerId == 0 {
		return nil, validator.ErrEmptyBannerID
	}

	err := rs.repo.DeleteByBannerID(ctx, r.BannerId)
	if err != nil {
		return nil, status.Errorf(
			codes.Internal,
			err.Error(),
		)
	}

	return &banner_rotation_proto.DeleteBannerResponse{}, nil
}

// Click handles a click on a banner in rotation.
func (rs *RotationService) Click(ctx context.Context, r *banner_rotation_proto.ClickRequest) (*banner_rotation_proto.ClickResponse, error) {
	if r == nil {
		return nil, errEmptyReq
	}

	if r.BannerId == 0 {
		return nil, validator.ErrEmptyBannerID
	}

	if r.SdgroupId == 0 {
		return nil, validator.ErrEmptySdgroupID
	}

	// Persist in a cache that we have a click
	_, err := rs.cache.IncrClicks(ctx, r.BannerId, r.SdgroupId)
	if err != nil {
		return nil, status.Errorf(
			codes.Internal,
			err.Error(),
		)
	}

	// Send a message about the click event to queue
	err = rs.aq.DispatchEvent(ctx, &analytics_proto.Event{
		BannerId:  r.BannerId,
		SdgroupId: r.SdgroupId,
		EventType: analytics_proto.EventType_CLICK,
		Date:      &timestamp.Timestamp{Seconds: time.Now().Unix(), Nanos: 0},
	})
	if err != nil {
		log.Printf("failed to dispatch an event about click: %+v\n", err)
	}

	// Record a metric.
	rs.mc.ClickCounter.Inc()

	return &banner_rotation_proto.ClickResponse{}, nil
}

// GetBanner gets a banner from rotation for given social demographic group in a slot.
func (rs *RotationService) GetBanner(ctx context.Context, r *banner_rotation_proto.GetBannerRequest) (*banner_rotation_proto.GetBannerResponse, error) {
	if r == nil {
		return nil, errEmptyReq
	}

	if r.SlotId == 0 {
		return nil, validator.ErrEmptySlotID
	}

	if r.SdgroupId == 0 {
		return nil, validator.ErrEmptySdgroupID
	}

	bannerID, err := rs.br.ResolveBanner(ctx, r.SlotId, r.SdgroupId)
	if err == resolver.ErrSlotWithoutBanners {
		return nil, status.Errorf(
			codes.NotFound,
			err.Error(),
		)
	}
	if err != nil {
		return nil, status.Errorf(
			codes.Internal,
			err.Error(),
		)
	}

	// Increment the total impressions count of the slot ID for the soc dem group.
	_, err = rs.cache.IncrTotalImpressions(ctx, r.SlotId, r.SdgroupId)
	if err != nil {
		return nil, status.Errorf(
			codes.Internal,
			err.Error(),
		)
	}

	// Persist in a cache that we have an impression of banner.
	_, err = rs.cache.IncrImpressions(ctx, bannerID, r.SdgroupId)
	if err != nil {
		return nil, status.Errorf(
			codes.Internal,
			err.Error(),
		)
	}

	// Send a message about the impression event to queue
	err = rs.aq.DispatchEvent(ctx, &analytics_proto.Event{
		BannerId:  bannerID,
		SlotId:    r.SlotId,
		SdgroupId: r.SdgroupId,
		EventType: analytics_proto.EventType_IMPRESSION,
		Date:      &timestamp.Timestamp{Seconds: time.Now().Unix(), Nanos: 0},
	})
	if err != nil {
		log.Printf("failed to dispatch an event about impression: %+v\n", err)
	}

	// Record a metric.
	rs.mc.ImpressionCounter.Inc()

	return &banner_rotation_proto.GetBannerResponse{
		BannerId: bannerID,
	}, nil
}
