package service

import (
	"context"
	"math/rand"
	"reflect"
	"testing"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"gitlab.com/forpelevin/banner-rotation/internal/metrics"

	"github.com/stretchr/testify/mock"
	"gitlab.com/forpelevin/banner-rotation/internal/cache"
	analytics_proto "gitlab.com/forpelevin/banner-rotation/internal/protobuf/analytics_service"
	banner_rotation_proto "gitlab.com/forpelevin/banner-rotation/internal/protobuf/banner_rotation_service"
	"gitlab.com/forpelevin/banner-rotation/internal/queue"
	"gitlab.com/forpelevin/banner-rotation/internal/repository"
	"gitlab.com/forpelevin/banner-rotation/internal/resolver"
)

var (
	resolvedBannerID = rand.Int()
	metricMock1      = promauto.NewCounter(prometheus.CounterOpts{Name: "first_mock"})
	metricMock2      = promauto.NewCounter(prometheus.CounterOpts{Name: "second_mock"})
)

type repositoryMock struct {
	repository.DB
	mock.Mock
}

func (m *repositoryMock) StoreBannerSlot(ctx context.Context, bs *repository.BannerSlot) error {
	return nil
}

func (m *repositoryMock) DeleteByBannerID(ctx context.Context, bannerID int64) error {
	return nil
}

type cacheMock struct {
	cache.Redis
	mock.Mock
}

func (r *cacheMock) IncrClicks(ctx context.Context, bannerID, sdgroupID int64) (int64, error) {
	return int64(rand.Int()), nil
}

func (r *cacheMock) IncrImpressions(ctx context.Context, bannerID, sdgroupID int64) (int64, error) {
	return int64(rand.Int()), nil
}

func (r *cacheMock) IncrTotalImpressions(ctx context.Context, slotID, sdgroupID int64) (int64, error) {
	return int64(rand.Int()), nil
}

type queueMock struct {
	queue.Kafka
	mock.Mock
}

func (k *queueMock) DispatchEvent(ctx context.Context, event *analytics_proto.Event) error {
	return nil
}

type bannerResolverMock struct {
	resolver.UCB1
	mock.Mock
}

func (u *bannerResolverMock) ResolveBanner(ctx context.Context, slotID, sdgroupID int64) (int64, error) {
	return int64(resolvedBannerID), nil
}

func getMockedService() *RotationService {
	rm := &repositoryMock{}
	cm := &cacheMock{}
	qm := &queueMock{}
	brm := &bannerResolverMock{}

	return NewRotationService(rm, cm, qm, brm, metrics.NewMetricCollector(
		metricMock1,
		metricMock2,
	))
}

func TestUnit_AddBanner(t *testing.T) {
	tests := []struct {
		name    string
		r       *banner_rotation_proto.AddBannerRequest
		want    *banner_rotation_proto.AddBannerResponse
		wantErr bool
	}{
		{
			name:    "empty request causes an error",
			r:       &banner_rotation_proto.AddBannerRequest{},
			want:    nil,
			wantErr: true,
		},
		{
			name: "invalid request without banner ID",
			r: &banner_rotation_proto.AddBannerRequest{
				SlotId: 1,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "invalid request without slot ID",
			r: &banner_rotation_proto.AddBannerRequest{
				BannerId: 1,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "positive case of creating",
			r: &banner_rotation_proto.AddBannerRequest{
				BannerId: 1,
				SlotId:   1,
			},
			want:    &banner_rotation_proto.AddBannerResponse{},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		s := getMockedService()
		t.Run(tt.name, func(t *testing.T) {
			got, err := s.AddBanner(context.Background(), tt.r)
			if (err != nil) != tt.wantErr {
				t.Errorf("RotationService.AddBanner() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("RotationService.AddBanner() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUnit_DeleteBanner(t *testing.T) {
	tests := []struct {
		name    string
		r       *banner_rotation_proto.DeleteBannerRequest
		want    *banner_rotation_proto.DeleteBannerResponse
		wantErr bool
	}{
		{
			name:    "empty request causes an error",
			r:       &banner_rotation_proto.DeleteBannerRequest{},
			want:    nil,
			wantErr: true,
		},
		{
			name: "positive case of deleting",
			r: &banner_rotation_proto.DeleteBannerRequest{
				BannerId: 1,
			},
			want:    &banner_rotation_proto.DeleteBannerResponse{},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		s := getMockedService()
		t.Run(tt.name, func(t *testing.T) {
			got, err := s.DeleteBanner(context.Background(), tt.r)
			if (err != nil) != tt.wantErr {
				t.Errorf("RotationService.DeleteBanner() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("RotationService.DeleteBanner() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUnit_Click(t *testing.T) {
	tests := []struct {
		name    string
		r       *banner_rotation_proto.ClickRequest
		want    *banner_rotation_proto.ClickResponse
		wantErr bool
	}{
		{
			name:    "empty request causes an error",
			r:       &banner_rotation_proto.ClickRequest{},
			want:    nil,
			wantErr: true,
		},
		{
			name: "invalid request without banner ID",
			r: &banner_rotation_proto.ClickRequest{
				SdgroupId: 1,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "invalid request without soc dem group ID",
			r: &banner_rotation_proto.ClickRequest{
				BannerId: 1,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "positive case of click",
			r: &banner_rotation_proto.ClickRequest{
				BannerId:  1,
				SdgroupId: 1,
			},
			want:    &banner_rotation_proto.ClickResponse{},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := getMockedService()
			got, err := s.Click(context.Background(), tt.r)
			if (err != nil) != tt.wantErr {
				t.Errorf("RotationService.Click() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("RotationService.Click() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUnit_GetBanner(t *testing.T) {
	tests := []struct {
		name    string
		r       *banner_rotation_proto.GetBannerRequest
		want    *banner_rotation_proto.GetBannerResponse
		wantErr bool
	}{
		{
			name:    "empty request causes an error",
			r:       &banner_rotation_proto.GetBannerRequest{},
			want:    nil,
			wantErr: true,
		},
		{
			name: "invalid request without slot ID",
			r: &banner_rotation_proto.GetBannerRequest{
				SdgroupId: 1,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "invalid request without soc dem group ID",
			r: &banner_rotation_proto.GetBannerRequest{
				SlotId: 1,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "positive case of getting a banner",
			r: &banner_rotation_proto.GetBannerRequest{
				SlotId:    1,
				SdgroupId: 1,
			},
			want: &banner_rotation_proto.GetBannerResponse{
				BannerId: int64(resolvedBannerID),
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := getMockedService()
			got, err := s.GetBanner(context.Background(), tt.r)
			if (err != nil) != tt.wantErr {
				t.Errorf("RotationService.GetBanner() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("RotationService.GetBanner() = %v, want %v", got, tt.want)
			}
		})
	}
}
