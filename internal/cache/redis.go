package cache

import (
	"context"
	"fmt"

	"github.com/go-redis/redis/v7"
)

// BannerCache is an abstraction for the cache that holds the banners stats.
// It used only for performance and realtime logic with active banners.
// You can see more detailed stats in OLAP storage where all impressions/clicks events are sent.
type BannerCache interface {
	// IncrClicks increments the clicks counter for the banner and soc dem group relation.
	IncrClicks(ctx context.Context, bannerID, sdgroupID int64) (int64, error)

	// IncrImpressions increments the impressions counter for the banner and soc dem group relation.
	IncrImpressions(ctx context.Context, bannerID, sdgroupID int64) (int64, error)

	// IncrTotalImpressions the total impressions count of the slot ID for the soc dem group.
	IncrTotalImpressions(ctx context.Context, slotID, sdgroupID int64) (int64, error)
}

// Redis is a banner cache that stores data in Redis.
type Redis struct {
	client *redis.Client
}

// NewRedis is a constructor.
func NewRedis(client *redis.Client) *Redis {
	return &Redis{client: client}
}

func getInt64(cmd *redis.StringCmd) (int64, error) {
	val, err := cmd.Int64()
	if err == redis.Nil {
		return val, nil
	}

	return val, err
}

func getBannerSdgroupClicksKey(bannerID, sdgroupID int64) string {
	return fmt.Sprintf("b:%d:sdg:%d:clicks", bannerID, sdgroupID)
}

func getBannerSdgroupImpressionsKey(bannerID, sdgroupID int64) string {
	return fmt.Sprintf("b:%d:sdg:%d:impressions", bannerID, sdgroupID)
}

func getSlotSdgroupTotalImpressionsKey(slotID, sdgroupID int64) string {
	return fmt.Sprintf("s:%d:sdg:%d:total", slotID, sdgroupID)
}

// IncrClicks increments the clicks counter for the banner and soc dem group relation.
func (r *Redis) IncrClicks(ctx context.Context, bannerID, sdgroupID int64) (int64, error) {
	key := getBannerSdgroupClicksKey(bannerID, sdgroupID)

	return r.client.Incr(key).Result()
}

// IncrImpressions increments the impressions counter for the banner and soc dem group relation.
func (r *Redis) IncrImpressions(ctx context.Context, bannerID, sdgroupID int64) (int64, error) {
	key := getBannerSdgroupImpressionsKey(bannerID, sdgroupID)

	return r.client.Incr(key).Result()
}

// IncrTotalImpressions the total impressions count of the slot ID for the soc dem group.
func (r *Redis) IncrTotalImpressions(ctx context.Context, slotID, sdgroupID int64) (int64, error) {
	key := getSlotSdgroupTotalImpressionsKey(slotID, sdgroupID)

	return r.client.Incr(key).Result()
}

// GetTotalImpressions returns the total impressions count in the slot for the soc dem group.
func (r *Redis) GetTotalImpressions(ctx context.Context, slotID, sdgroupID int64) (int64, error) {
	key := getSlotSdgroupTotalImpressionsKey(slotID, sdgroupID)

	return getInt64(r.client.Get(key))
}

// GetBannerClicks returns a clicks count of the banner for the soc dem group.
func (r *Redis) GetBannerClicks(ctx context.Context, bannerID, sdgroupID int64) (int64, error) {
	key := getBannerSdgroupClicksKey(bannerID, sdgroupID)

	return getInt64(r.client.Get(key))
}

// GetBannerImpressions returns an impressions count of the banner for the soc dem group.
func (r *Redis) GetBannerImpressions(ctx context.Context, bannerID, sdgroupID int64) (int64, error) {
	key := getBannerSdgroupImpressionsKey(bannerID, sdgroupID)

	return getInt64(r.client.Get(key))
}
