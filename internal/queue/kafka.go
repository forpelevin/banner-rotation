package queue

import (
	"context"
	"fmt"
	"log"

	"github.com/golang/protobuf/proto"
	"github.com/pkg/errors"
	"github.com/segmentio/kafka-go"
	analytics_proto "gitlab.com/forpelevin/banner-rotation/internal/protobuf/analytics_service"
)

const (
	AnalyticsEventTopic = "banner_rotation.analytics.event"
)

// AnalyticsQueue is a bus for the Stat events.
type AnalyticsQueue interface {
	// DispatchEvent dispatches the given analytics event to a bus.
	DispatchEvent(ctx context.Context, event *analytics_proto.Event) error
}

// Kafka implements the Analytics queue for stats data.
type Kafka struct {
	kw *kafka.Writer
}

// NewKafka is a constructor.
func NewKafka(kw *kafka.Writer) *Kafka {
	return &Kafka{kw: kw}
}

// DispatchEvent dispatches the given analytics event to a bus.
func (k *Kafka) DispatchEvent(ctx context.Context, e *analytics_proto.Event) error {
	// Encode the message body using protobuf
	value, err := proto.Marshal(e)
	if err != nil {
		log.Fatal(err)
	}

	msg := kafka.Message{
		Key:   getKeyByEvent(e),
		Value: value,
	}

	err = k.kw.WriteMessages(ctx, msg)
	if err != nil {
		return errors.Wrap(err, "failed to dispatch an analytics event in Kafka")
	}

	return nil
}

func getKeyByEvent(e *analytics_proto.Event) []byte {
	return []byte(fmt.Sprintf(
		"%d:%d:%d:%d:%d",
		e.BannerId,
		e.SlotId,
		e.EventType,
		e.SdgroupId,
		e.Date.Seconds,
	))
}
