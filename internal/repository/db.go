package repository

import (
	"context"
	"database/sql"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
)

const (
	insertBannerSlotQuery           = `INSERT INTO banner_slot (banner_id, slot_id) VALUES (?, ?)`
	deleteBannerSlotByBannerIDQuery = `DELETE FROM banner_slot WHERE banner_id = ?`
	selectOneBannerBySlotID         = `SELECT banner_id FROM banner_slot WHERE slot_id = ? LIMIT 1`
	selectBannersBySlotID           = `SELECT banner_id FROM banner_slot WHERE slot_id = ?`
)

var (
	ErrNotFound = errors.New("not found")
)

// BannerRotationRepository is an interface of the BannerSlot model storage.
type BannerRotationRepository interface {
	// StoreBannerSlot stores the banner slot relation in a storage.
	StoreBannerSlot(ctx context.Context, bs *BannerSlot) error

	// DeleteByBannerID deletes the banner slot relation from a storage.
	DeleteByBannerID(ctx context.Context, bannerID int64) error
}

// BannerSlot is a model to store the banner and slot relation.
type BannerSlot struct {
	BannerID int64 `db:"banner_id"`
	SlotID   int64 `db:"slot_id"`
}

// DB is a banner rotation repository that stores data in DB.
type DB struct {
	db *sqlx.DB
}

// NewDB is a constructor.
func NewDB(db *sqlx.DB) *DB {
	return &DB{db: db}
}

// StoreBannerSlot stores the banner slot relation in a storage.
func (d *DB) StoreBannerSlot(ctx context.Context, bs *BannerSlot) error {
	_, err := d.db.ExecContext(ctx, insertBannerSlotQuery, bs.BannerID, bs.SlotID)
	return err
}

// DeleteByBannerID deletes the banner slot relation from a storage.
func (d *DB) DeleteByBannerID(ctx context.Context, bannerID int64) error {
	_, err := d.db.ExecContext(ctx, deleteBannerSlotByBannerIDQuery, bannerID)
	return err
}

// GetOneBannerBySlotID returns a single banner ID by the slot.
func (d *DB) GetOneBannerBySlotID(ctx context.Context, slotID int64) (int64, error) {
	var bannerID int64
	row := d.db.QueryRowContext(ctx, selectOneBannerBySlotID, slotID)
	err := row.Scan(&bannerID)
	if err != nil && err == sql.ErrNoRows {
		return bannerID, ErrNotFound
	}
	if err != nil {
		return 0, errors.Wrap(err, "failed to get a one banner by slot ID from DB")
	}

	return bannerID, nil
}

// GetBannersBySlotID returns all banners that are related to the slot.
func (d *DB) GetBannersBySlotID(ctx context.Context, slotID int64) ([]int64, error) {
	rows, err := d.db.QueryxContext(ctx, selectBannersBySlotID, slotID)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get banners by slot ID from DB")
	}

	bannerIDs := make([]int64, 0)
	for rows.Next() {
		var bannerID int64
		err := rows.Scan(&bannerID)
		if err != nil {
			return nil, err
		}

		bannerIDs = append(bannerIDs, bannerID)
	}

	return bannerIDs, nil
}
