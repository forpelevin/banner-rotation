package database

import (
	"fmt"
	"os"
	"time"

	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
)

const (
	DbMAxOpenConns                 = 5
	DbMaxIdleConnections           = 100
	DbConnectionMaxLifetimeSeconds = 300 * time.Second
)

// NewDbNotifier creates a new instance of the DBNotifier using ENV params.
func NewDBConnection() (*sqlx.DB, error) {
	dsn := fmt.Sprintf(
		"%s:%s@tcp(%s:%s)/%s",
		os.Getenv("DB_USERNAME"),
		os.Getenv("DB_PASSWORD"),
		os.Getenv("DB_HOST"),
		os.Getenv("DB_PORT"),
		os.Getenv("DB_NAME"),
	)
	fmt.Println(dsn)
	db, err := sqlx.Connect("mysql", dsn)
	if err != nil {
		return nil, errors.Wrap(err, "failed to connect to DB")
	}
	db.SetMaxOpenConns(DbMAxOpenConns)
	db.SetMaxIdleConns(DbMaxIdleConnections)
	db.SetConnMaxLifetime(DbConnectionMaxLifetimeSeconds)

	return db, nil
}
