package resolver

import (
	"math"
	"testing"
)

func TestUnit_calculateScore(t *testing.T) {
	tests := []struct {
		name string
		xj   int64
		n    int64
		nj   int64
		want float64
	}{
		{
			name: "score is 0 if only the banner was shown",
			xj:   0,
			n:    1,
			nj:   1,
			want: 0,
		},
		{
			name: "usual score without clicks",
			xj:   0,
			n:    10,
			nj:   1,
			want: 2.145966026289347,
		},
		{
			name: "same as previous case but we got a click and we should have a bigger score.",
			xj:   1,
			n:    10,
			nj:   1,
			want: 3.145966026289347,
		},
		{
			name: "if a banner was not shown at least once, it will have a plus infinite score",
			xj:   1,
			n:    10,
			nj:   0,
			want: math.Inf(1),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := calculateScore(tt.xj, tt.n, tt.nj); got != tt.want {
				t.Errorf("calculateScore() = %v, want %v", got, tt.want)
			}
		})
	}
}
