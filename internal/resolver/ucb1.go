package resolver

import (
	"context"
	"math"

	"github.com/pkg/errors"
)

var (
	ErrSlotWithoutBanners = errors.New("the slot has no banners in rotation")
)

// BannerRepository is an abstraction of a long term storage of banner and slot relations.
type BannerRepository interface {
	// GetOneBannerBySlotID returns a single banner ID by the slot.
	GetOneBannerBySlotID(ctx context.Context, slotID int64) (int64, error)

	// GetBannersBySlotID returns all banners that are related to the slot.
	GetBannersBySlotID(ctx context.Context, slotID int64) ([]int64, error)
}

// BannerCache is an abstraction of a fast storage of banners stats.
type BannerCache interface {
	// GetTotalImpressions returns the total impressions count in the slot for the soc dem group.
	GetTotalImpressions(ctx context.Context, slotID, sdgroupID int64) (int64, error)

	// GetBannerClicks returns a clicks count of the banner for the soc dem group.
	GetBannerClicks(ctx context.Context, bannerID, sdgroupID int64) (int64, error)

	// GetBannerImpressions returns an impressions count of the banner for the soc dem group.
	GetBannerImpressions(ctx context.Context, bannerID, sdgroupID int64) (int64, error)
}

// BannerResolver is an abstraction that resolves and returns a banner for showing.
type BannerResolver interface {
	// ResolveBanner resolves which banner should be shown for given soc dem group in the slot.
	ResolveBanner(ctx context.Context, slotID, sdgroupID int64) (int64, error)
}

// UCB1 is a banner resolver that resolves banners using the UCB1; algorithm.
type UCB1 struct {
	repo  BannerRepository
	cache BannerCache
}

// NewUCB1 is a constructor.
func NewUCB1(repo BannerRepository, cache BannerCache) *UCB1 {
	return &UCB1{
		repo:  repo,
		cache: cache,
	}
}

// ResolveBanner resolves which banner should be shown for given soc dem group in the slot.
func (u *UCB1) ResolveBanner(ctx context.Context, slotID, sdgroupID int64) (int64, error) {
	// How many times the banners were shown in slot. Also, the count will be used in future calculations.
	totalImpressions, err := u.cache.GetTotalImpressions(ctx, slotID, sdgroupID)
	if err != nil {
		return 0, errors.Wrap(err, "failed to fetch the total impressions count from cache on banner resolving")
	}

	// Take all banners of the Slot.
	banners, err := u.repo.GetBannersBySlotID(ctx, slotID)
	if err != nil {
		return 0, errors.Wrap(err, "failed to get banners from repo on banner resolving")
	}

	if len(banners) == 0 {
		return 0, ErrSlotWithoutBanners
	}

	// Calculate a show score for each banner.
	bannerWithBiggestScore := banners[0]
	var biggestScore float64 = 0
	for _, bannerID := range banners {
		bannerImpressions, err := u.cache.GetBannerImpressions(ctx, bannerID, sdgroupID)
		if err != nil {
			return 0, errors.Wrap(err, "failed to get banner impressions from cache on banner resolving")
		}

		// If the banner was not shown at least once, we should show it.
		if bannerImpressions < 1 {
			return bannerID, nil
		}

		bannerClicks, err := u.cache.GetBannerClicks(ctx, bannerID, sdgroupID)
		if err != nil {
			return 0, errors.Wrap(err, "failed to get banner clicks from cache on banner resolving")
		}

		currentBannerScore := calculateScore(bannerClicks, totalImpressions, bannerImpressions)
		// If current banner score is Inf+ (highest available score) we don't need to calculate next banners.
		if math.IsInf(currentBannerScore, 1) {
			return bannerID, nil
		}

		if currentBannerScore > biggestScore {
			bannerWithBiggestScore = bannerID
			biggestScore = currentBannerScore
		}
	}

	// Return a banner with the biggest score.
	return bannerWithBiggestScore, nil
}

// calculateScore calculates a score for the params.
// xj - average profit of banner. In our case, this is clicks count.
// n - total impressions count of all banners.
// nj - impressions count of current banner.
func calculateScore(xj, n, nj int64) float64 {
	return float64(xj) + math.Sqrt(2*math.Log(float64(n))/float64(nj))
}
