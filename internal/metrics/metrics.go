package metrics

import (
	"github.com/prometheus/client_golang/prometheus"
)

// MetricCollector is a struct that wraps the Prometheus custom metrics
type MetricCollector struct {
	ClickCounter      prometheus.Counter
	ImpressionCounter prometheus.Counter
}

// NewMetricCollector creates a new instance of the MetricCollector with predefined metrics.
func NewMetricCollector(cc, ic prometheus.Counter) *MetricCollector {
	return &MetricCollector{
		ClickCounter:      cc,
		ImpressionCounter: ic,
	}
}
