package integration

import (
	"context"
	"math/rand"
	"sync"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	banner_rotation_proto "gitlab.com/forpelevin/banner-rotation/internal/protobuf/banner_rotation_service"
)

// After a lot of GetBanner request we ought to receive each banner at least once.
// All testing within one slot.
func TestIntegration_Rotation(t *testing.T) {
	rand.Seed(time.Now().Unix())
	rsc := GetRotationServiceClient()

	// Add 10 banners to rotation.
	slotID := int64(rand.Int())
	sdgroupID := int64(rand.Int())
	banners := make([]int64, 0, 10)
	for i := 0; i < 10; i++ {
		bannerID := int64(rand.Int())
		_, err := rsc.AddBanner(context.Background(), &banner_rotation_proto.AddBannerRequest{
			SlotId:   slotID,
			BannerId: bannerID,
		})
		require.NoError(t, err)

		banners = append(banners, bannerID)
	}

	// Call the GetBanner method 8000 times.
	mx := sync.Mutex{}
	wg := sync.WaitGroup{}
	shownBannersMap := make(map[int64]bool, 10)
	for i := 0; i < 100; i++ {
		wg.Add(1)
		go func() {
			bannerID := GetBanner(t, slotID, sdgroupID)
			mx.Lock()
			shownBannersMap[bannerID] = true
			mx.Unlock()
			wg.Done()
		}()
	}
	wg.Wait()

	// Check that each banner was returned at least once.
	for _, bannerID := range banners {
		_, ok := shownBannersMap[bannerID]
		if !ok {
			t.Errorf("the banner with ID %d was not shown", bannerID)
		}
	}
}

// GetBanner calls the RotationService and returns a banner ID for the given slot.
func GetBanner(t *testing.T, slotID, sdgroupID int64) int64 {
	rsc := GetRotationServiceClient()

	getBannerRes, err := rsc.GetBanner(context.Background(), &banner_rotation_proto.GetBannerRequest{
		SlotId:    slotID,
		SdgroupId: sdgroupID,
	})
	require.NoError(t, err)

	return getBannerRes.BannerId
}
