package integration

import (
	"context"
	"fmt"
	"math/rand"
	"os"
	"testing"
	"time"

	"github.com/golang/protobuf/proto"
	"github.com/segmentio/kafka-go"
	"github.com/stretchr/testify/require"
	analytics_proto "gitlab.com/forpelevin/banner-rotation/internal/protobuf/analytics_service"
	banner_rotation_proto "gitlab.com/forpelevin/banner-rotation/internal/protobuf/banner_rotation_service"
	"gitlab.com/forpelevin/banner-rotation/internal/queue"
)

func TestIntegration_AnalyticsQueue(t *testing.T) {
	rsc := GetRotationServiceClient()

	kafkaReader := kafka.NewReader(kafka.ReaderConfig{
		Brokers: []string{fmt.Sprintf("%s:%s", os.Getenv("KAFKA_HOST"), os.Getenv("KAFKA_PORT"))},
		Topic:   queue.AnalyticsEventTopic,
	})
	defer kafkaReader.Close()

	// When a user performs a click, a due event will be sent in the Analytics queue.
	rand.Seed(time.Now().Unix())
	bannerID := int64(rand.Int())
	sdgroupID := int64(rand.Int())
	slotID := int64(rand.Int())

	_, err := rsc.AddBanner(context.Background(), &banner_rotation_proto.AddBannerRequest{
		SlotId:   slotID,
		BannerId: bannerID,
	})
	require.NoError(t, err)

	MakeClick(t, bannerID, sdgroupID)

	e := ReadEvent(t, kafkaReader, bannerID)
	require.NotNil(t, e)
	require.Equal(t, bannerID, e.BannerId)
	require.Equal(t, sdgroupID, e.SdgroupId)
	require.Equal(t, analytics_proto.EventType_CLICK, e.EventType)

	// When a user gets a banner, a due event also will be sent in the Analytics queue.
	GetBanner(t, slotID, sdgroupID)
	e = ReadEvent(t, kafkaReader, bannerID)
	require.NotNil(t, e)
	require.Equal(t, bannerID, e.BannerId)
	require.Equal(t, sdgroupID, e.SdgroupId)
	require.Equal(t, slotID, e.SlotId)
	require.Equal(t, analytics_proto.EventType_IMPRESSION, e.EventType)
}

func ReadEvent(t *testing.T, kr *kafka.Reader, bannerID int64) *analytics_proto.Event {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	// We read messages until we find our banner or we will drop by timeout.
	for {
		m, err := kr.ReadMessage(ctx)
		if err != nil {
			t.Error(err)
		}

		// Ignore the empty messages.
		if len(m.Value) == 0 {
			return nil
		}

		var e analytics_proto.Event
		err = proto.Unmarshal(m.Value, &e)
		require.NoError(t, err)

		if e.BannerId != bannerID {
			continue
		}

		return &e
	}
}
