package integration

import (
	"context"
	"math/rand"
	"testing"

	"github.com/stretchr/testify/require"
	banner_rotation_proto "gitlab.com/forpelevin/banner-rotation/internal/protobuf/banner_rotation_service"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// Test the feature of adding/deleting a banner from rotation.
func TestIntegration_AddOrDeleteBanner(t *testing.T) {
	rsc := GetRotationServiceClient()

	// Add a banner to rotation.
	slotID := int64(rand.Int())
	bannerID := int64(rand.Int())
	sdgroupID := int64(rand.Int())
	_, err := rsc.AddBanner(context.Background(), &banner_rotation_proto.AddBannerRequest{
		SlotId:   slotID,
		BannerId: bannerID,
	})
	require.NoError(t, err)

	// Check that the banner is in.
	bannerIDInRotation := GetBanner(t, slotID, sdgroupID)
	require.Equal(t, bannerID, bannerIDInRotation)

	// Delete a banner from rotation.
	_, err = rsc.DeleteBanner(context.Background(), &banner_rotation_proto.DeleteBannerRequest{
		BannerId: bannerID,
	})
	require.NoError(t, err)

	// Check that the rotation is empty.
	_, err = rsc.GetBanner(context.Background(), &banner_rotation_proto.GetBannerRequest{
		SlotId:    slotID,
		SdgroupId: sdgroupID,
	})
	require.NotNil(t, err)

	e, ok := status.FromError(err)
	if !ok {
		t.Fatal(err)
	}

	if e.Code() != codes.NotFound {
		t.Fatal(err)
	}
}
