package integration

import (
	"fmt"
	"log"
	"os"
	"sync"
	"testing"

	"github.com/joho/godotenv"
	banner_rotation_proto "gitlab.com/forpelevin/banner-rotation/internal/protobuf/banner_rotation_service"
	"google.golang.org/grpc"
)

var (
	rsc  banner_rotation_proto.RotationServiceClient
	once sync.Once
)

func TestMain(m *testing.M) {
	// if we crash the go code, we get the file name and line number
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	// Read the env file.
	err := godotenv.Load("./../../.env")
	if err != nil {
		log.Fatal(err)
	}

	code := m.Run()
	os.Exit(code)
}

// GetRotationServiceClient creates a new rotation service client once and returns it.
func GetRotationServiceClient() banner_rotation_proto.RotationServiceClient {
	once.Do(func() {
		// Connect to the gRPC server via TCP.
		address := fmt.Sprintf(
			"%s:%s",
			os.Getenv("BANNER_GRPC_HOST"),
			os.Getenv("BANNER_GRPC_PORT"),
		)
		fmt.Printf("Connecting to address: %s\n", address)
		conn, err := grpc.Dial(address, grpc.WithInsecure())
		if err != nil {
			log.Fatal(err)
		}

		rsc = banner_rotation_proto.NewRotationServiceClient(conn)
	})

	return rsc
}
