package integration

import (
	"context"
	"fmt"
	"math/rand"
	"sync"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	banner_rotation_proto "gitlab.com/forpelevin/banner-rotation/internal/protobuf/banner_rotation_service"
)

func TestIntegration_PopularClick(t *testing.T) {
	rsc := GetRotationServiceClient()

	// Given we have 3 banners in rotation.
	rand.Seed(time.Now().Unix())
	slotID := int64(rand.Int())
	sdgroupID := int64(rand.Int())
	banners := make([]int64, 0, 3)
	for i := 0; i < 3; i++ {
		bannerID := int64(rand.Int())
		_, err := rsc.AddBanner(context.Background(), &banner_rotation_proto.AddBannerRequest{
			SlotId:   slotID,
			BannerId: bannerID,
		})
		require.NoError(t, err)

		banners = append(banners, bannerID)
	}

	thirdBannerID := banners[2]
	// We have 1 click for each banner.
	for _, bannerID := range banners {
		MakeClick(t, bannerID, sdgroupID)

		// The third banner has another 2 clicks.
		if bannerID == thirdBannerID {
			MakeClick(t, bannerID, sdgroupID)
			MakeClick(t, bannerID, sdgroupID)
		}
	}

	// Call the GetBanner method 1000 times.
	mx := sync.Mutex{}
	wg := sync.WaitGroup{}
	shownBannersCounterMap := make(map[int64]int, 3)
	for i := 0; i < 1000; i++ {
		wg.Add(1)
		go func() {
			bannerID := GetBanner(t, slotID, sdgroupID)
			mx.Lock()
			shownBannersCounterMap[bannerID]++
			mx.Unlock()
			wg.Done()
		}()
	}
	wg.Wait()

	// And the third banner must be shown in most of cases.
	thirdBannerShownTimes := shownBannersCounterMap[thirdBannerID]
	fmt.Printf("Banner show map: %+v\n", shownBannersCounterMap)
	if thirdBannerShownTimes < 900 {
		t.Errorf("expected that the banner was shown more than in 90 percent of cases, but it's not")
	}
}

func MakeClick(t *testing.T, bannerID int64, sdgroupID int64) {
	rsc := GetRotationServiceClient()

	_, err := rsc.Click(context.Background(), &banner_rotation_proto.ClickRequest{
		BannerId:  bannerID,
		SdgroupId: sdgroupID,
	})
	require.NoError(t, err)
}
