CREATE UNIQUE INDEX IF NOT EXISTS unique_banner_slot_idx ON banner_slot(banner_id, slot_id);
CREATE INDEX IF NOT EXISTS slot_idx ON banner_slot(slot_id);